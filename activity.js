/*
VON ARCEO
s28 Activity
*/


// ACTIVITY NO. 3
db.room.insert({
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	room_available: 10,
	isAvailable: false
})
//ACTIVITY NO.3 END

//ACTIVITY NO.4
db.room.insertMany([
	{
	name: "Double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	room_available: 5,
	isAvailable: false
	},
	{
	name: "Queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a single getaway",
	room_available: 15,
	isAvailable: false
	}
])

//ACTIVITY NO.4 END 

// ACTIVITY NO.5
db.room.find({ name: "Double" })
// ACTIVITY NO.5 END

//ACTIVITY NO.6
db.room.updateOne({
	name: "Queen"
},
	{
		$set: {
			room_available: 0
		}
	}
)
//ACTIVITY NO.6 END

//ACTIVITY NO.7

db.room.deleteMany({
	room_available: 0
})

//ACTIVITY NO.7 END

